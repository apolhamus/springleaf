from __future__ import absolute_import
import platform

data_path = '/home/darryl/src/spring/data'

train_data = '/home/darryl/src/spring/data/train_head.csv'
test_data = '/home/darryl/src/spring/data/test_head.csv'
out_path = '/tmp'
data_out_path = "/".join([data_path, 'devdata'])
test_continuous_fname = "/".join([data_path, 'trainsep23_5pm.csv'])
train_continuous_fname = "/".join([data_path, 'trainsep23_5pm.csv'])

if platform.node() == "Frederick": # grayson computer name...
    data_path =    '/Users/aaronpolhamus/Dropbox/Polhooper/LeadScore/springleaf/data'
    data_out_path = "/".join([data_path, 'devdata'])
    train_data = "/".join([data_path, "train.csv"])
    test_data = "/".join([data_path, "test.csv"])
    out_path = '/tmp'
    
    train_file_csv = "/".join([data_path, 'devdata/slice_0.csv'])
    test_file_csv = "/".join([data_path, 'devdata/slice_1.csv'])
    train_file_vw = "/".join([data_path,'devdata/slice_0.vw'])
    test_file_vw = "/".join([data_path, 'devdata/slice_1.vw'])
    
    models_directory = "models"
    test_continuous_fname = "/".join([data_path, 'testsep23_5pm.csv'])
    train_continuous_fname = "/".join([data_path, 'trainsep23_5pm.csv'])
else:
    train_data = "~/data/train.csv"
    test_data = "~/data/test.csv"
    out_dir = "/tmp"
