import sys, subprocess
import pandas as pd 
import numpy as np 

base_dir = '/home/ubuntu/prodrun/concept2'

for run in [1, 2, 3, 4, 5, 6]:
    for tag in ['a', 'b']:
        run_dir = '%s/run%d' % (base_dir, run)
        run_sub_dir = '%s/run%s' % (run_dir, tag) 
        
        print(run_sub_dir)
        
        if tag == 'a':
            data_file = '%s/slice_2.csv' % run_dir
        else:  
            data_file = '%s/slice_1.csv' % run_dir 

        print('scanning train...')
        df = pd.read_csv(data_file)
        xgb_preds = pd.read_csv('%s/xgb.csv' % run_sub_dir)
        rf_preds = np.loadtxt('%s/rf_preds_reg.txt' % run_sub_dir)
        
        df['xgb'] = list(xgb_preds['target'])
        df['rf'] = list(rf_preds)
        
        feature_names = [x for x in df.columns.values if x not in ['target', 'ID', 'xgb', 'rf']] 
        features_to_write = ['target', 'xgb', 'rf'] + feature_names
        df = df[features_to_write]
        
        msk = np.random.rand(len(df)) < 0.75
        train = df[msk]
        test = df[~msk]
        
        print('writing subsets....')
        train.to_csv('%s/train.csv' % run_sub_dir, headers = True, index = False)
        test.to_csv('%s/test.csv' % run_sub_dir, headers = True, index = False)

        val_df = df[['target', 'xgb', 'rf']]
        val_df.to_csv('%s/val_df.csv' % run_sub_dir, headers = True, index = False)
