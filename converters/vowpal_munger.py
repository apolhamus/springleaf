import csv

#..TODO (Grayson) : Add a bit of logic that auto-detects if y has the values [0, 1] 
#  and re-assigns values [-1, 1] if so. 

class VWEncoder(object):
    def __init__(self, in_fname):
        self.load_data(in_fname)
        self.build_template()

    def load_data(self, fname):
        self.reader = csv.reader(open(fname), delimiter=",")
        self.header = next(self.reader)

    def namespaces(self):
        """ namespaces is a tuple of tuples: ((namespace, (start_col, end_col)),...)
        requires to start 'y' namespace
        """
        pass

    def build_template(self):
        """ namespaces is a tuple of tuples: ((namespace, (start_col, end_col)),...)
        requires to start 'y' namespace
        """
        namespaces = self.namespaces()
        template = []
        for name, limits in namespaces:
            space_store = []
            start, end = limits

            if start == end:
                col_nums = [start]
                var_names = [self.header[start]]
            else:
                col_nums = range(start,end + 1) 
                var_names = self.header[start:end + 1]

            if name == 'y':
                if len(var_names) > 1:
                    space_store.append("{0} '{1}")
                else:
                    space_store.append("{0}")

            else:
                for var_name, col_num in zip(var_names, col_nums):
                    space_store.append("%s:{%d}" % (var_name, col_num))
    
            features = " ".join(space_store)
            if name != 'y':
                features = " ".join([name, features])

            template.append(features)

        self.template = " |".join(template)

    def encode_row(self, row):
        return self.template.format(*row)
        
    def encode(self, fname):
        with open(fname, 'wb') as f:
            for row in self.reader:
                encoded_row = self.encode_row(row)
                f.write(bytes("%s\n" % encoded_row, 'UTF-8'))

class SpringLeafConcept1(VWEncoder): 
    def namespaces(self): 
        return (('y', (0, 0)), ('num', (1, 255)), ('cat', (256, 345)), ('pca', (346, 357)) )

class SpringLeafConcept2(VWEncoder): 
    def namespaces(self): 
        return (('y', (0, 0)), ('num', (1, 255)), ('cat', (256, 343)))

class SpringLeafConcept2_2(VWEncoder): 
    def namespaces(self): 
        return (('y', (0, 0)), ('num', (1, 255)), ('cat', (256, 343)), ('forecasts', (344, 349)))

class SpringLeafEnsembler(VWEncoder): 
    def namespaces(self): 
        return (('y', (0, 0)), ('predictions', (1, 6)) )
        
class SpringLeaf(VWEncoder): 
    def namespaces(self): 
        return (('y', (0, 0)), ('predictions', (1, 2)), ('features', (3, 1198)) )
