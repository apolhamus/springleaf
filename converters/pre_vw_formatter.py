#!/usr/bin/env python

import subprocess, sys, os, pandas, csv, numpy

def missing_value_generator(seq_len=12):
    """ Generate all possible missing values for Springleaf data

    """
    missing_vals = [-1, -99999]
    i = 0
    while i < seq_len:
        try:
            base_val = prev_val + 10 ** i
        except:
            base_val = 1
        i += 1
        
        yield_val = 9 * base_val
        prev_val = base_val
        for suffix in range(10):
            # The line below is amazing 'int' concatinating...
            val = int(str(yield_val) + str(suffix)) 
            if val not in range(90,98): # think these are valid values
                missing_vals.append(val)
    
    return missing_vals

missing_vals = missing_value_generator()

def match_on_mean(indata, missing_list):
    for var in list(indata.columns.values):
        print('formatting variable %s...') % var
        unique_vals = list(indata[var].unique())
        if len(unique_vals) > 2: 
            vals_to_treat = []
            for val in unique_vals: 
                if val in missing_list: 
                    vals_to_treat.append(val)
            if len(vals_to_treat) > 0:
                mean_table = indata[['target', var]].groupby(var)
                mean_table = mean_table.mean()
                
                real_vals = [x for x in unique_vals if x not in vals_to_treat]
                missing_vals = [x for x in unique_vals if x in vals_to_treat]
                
                missing_table = mean_table[mean_table.index.isin(missing_vals)] 
                real_table = mean_table[mean_table.index.isin(real_vals)]
                
                vec = list(indata[var])
                
                print('making subs...')
                for val in vals_to_treat: 
                    target_val = float(missing_table[missing_table.index.isin([val])]['target'])
                    real_table['diffseries'] = numpy.absolute([x - target_val for x in real_table['target']])
                    min_val = min(real_table['diffseries'])
                    replace_val = real_table[real_table.diffseries.isin([min_val])]
                    replace_val = replace_val.index[0]
                    for i in range(len(vec)):
                        if vec[i] == val: 
                            vec[i] = replace_val 
                
                indata[var] = vec
    return(indata)

def main():
    in_file = sys.argv[1]
    out_file = sys.argv[2]
    
    print('scan in raw data...')
    data = pandas.read_csv(in_file)
    missing_vals = missing_value_generator()
    print('replace missing values...')
    output = match_on_mean(data, missing_vals)
    output = output.rename(columns = {'target':'y'})
    new_target = list(output['y'])
    for i in range(len(new_target)): 
        if new_target[i] == 0: 
            new_target[i] = -1
    output['y'] = new_target
    print('write output to file...')
    output.to_csv(out_file, index = False, header = True)

if __name__ == '__main__':
    main()
