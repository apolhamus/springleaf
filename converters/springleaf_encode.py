import os, glob, sys
import vowpal_munger

file_path = sys.argv[1]

encoder = vowpal_munger.SpringLeaf(file_path)
encoder.encode(file_path[:-4] + ".vw")
