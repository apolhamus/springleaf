import pandas as pd
import os
import numpy as np
import random
import subprocess
import cPickle as pickle
import luigi
from sklearn.preprocessing import LabelEncoder
import gc

import settings.common as cfg

def basename(fname):
    with_prefix = os.path.basename(fname)
    wo_prefix = with_prefix.rsplit(".")[0]
    return wo_prefix

def presence_absence(data):
    data[data > 1] = 1
    data = data.fillna(2)
    return data

def missing_value_generator(seq_len=12):
    """ Generate all possible missing values for Springleaf data

    """
    missing_vals = [-1, -99999]
    i = 0
    while i < seq_len:
        try:
            base_val = prev_val + 10 ** i
        except:
            base_val = 1
        i += 1
        
        yield_val = 9 * base_val
        prev_val = base_val
        for suffix in range(10):
            # The line below is amazing 'int' concatinating...
            val = int(str(yield_val) + str(suffix)) 
            if val not in range(90,98): # think these are valid values
                missing_vals.append(val)
    
    return missing_vals

class PresenceAbsence(luigi.Task):

    fname = luigi.Parameter()

    def output(self):
        bname = basename(self.fname)
        suffix = "pa.csv"
        new_name = "_".join([bname, suffix]) 
        out_fname = "/".join([cfg.out_path, new_name])
        return luigi.LocalTarget(out_fname)

    def requires(self):
        return RawData(fname=self.fname)

    def filter_nunique(self, nunique=200):
        """ Return list of cols w < nunique values
        """
        unique_by_col = self.data.apply(pd.Series.nunique)

        cols = [col_name for col_name, col_ct in unique_by_col.iteritems() if col_ct >= nunique]
        return cols
    
    def filter_type(self, return_type='str'):
        """ Hatchet job to filter cols by type...
        """
        if return_type == 'str':
            dmax = self.data.max()
            return [col_name for col_name, val in dmax.iteritems() if isinstance(val, str)]


    def bad_columns(self):
        cols = [8, 9, 10, 11, 12, 43, 196, 221, 230, 239, 44, 214]
        return ["VAR_%04d" % col for col in cols]

    def get_unvariant_cols(self):
        unique_values = self.data.apply(pd.Series.nunique)
        return [col_name for col_name, nunique in unique_values.iteritems() if nunique == 1]

    def get_all_exclusions(self):
        str_cols = self.filter_type()
        bad_cols = self.bad_columns()
        univar_cols = self.get_unvariant_cols()
        cont_cols = self.filter_nunique()
        return str_cols + bad_cols + univar_cols + cont_cols

    def replace_na_with_nan(self):
        to_replace = missing_value_generator()
        self.data = self.data.replace(value=np.nan, to_replace=to_replace)
         
    def run(self):
        self.data = pd.read_csv(self.input().path)

        exclude_cols =  self.get_all_exclusions()
        self.data = self.data.drop(exclude_cols, axis=1)

        self.replace_na_with_nan()
        self.data = self.data.apply(presence_absence, axis=1)

        more_exclude_cols =  self.get_unvariant_cols()
        self.data = self.data.drop(more_exclude_cols, axis=1)
        
        self.data.columns = ["%s_PA" % x for x in self.data.columns.values.tolist()] # rename so no confliect w other versions

        self.data.to_csv(self.output().path, index=False)



class RawData(luigi.ExternalTask):
    """ Input files
    """
    fname = luigi.Parameter()

    def output(self):
        return luigi.LocalTarget(self.fname)

class GenerateContinuousData(luigi.Task):
    """ Execute R scripts to deal with continuous (test and train)

    Kind of a bastardization as going to output two files whatever
    """
    def requires(self):
        return RawData(cfg.train_data)

    def output(self):
        """ assume if train is done that also have test...
        """
        return luigi.LocalTarget(cfg.test_continuous_fname)

    def run(self):
        subprocess.call("Rscript generate_continuous.R", shell=True)
        
class GenerateCategoricalEncoders(luigi.Task):
    """ This would be much better columnwise reading from a database me thinks
    """

    def get_powerful_cols(self):
        """ Loads list of categorical vars we think might be useful

        This is all based on just manual looking right now

        """
        two_cols = [232, 292,362,383,384,401,469,471,490,503,504,505]
        three_cols = [1, 122, 183, 252, 281, 285, 286, 287, 291, 292, 306, 307, 308, 312, 326, 345, 346, 347, 401,
                402, 507, 513, 520, 723, 1175, 1176]
        four_cols = [5, 116, 150, 153, 163, 173, 250, 251, 290, 311, 352, 420, 467, 472, 508, 809, 994,
                1557, 1562]
        five_cols = [45, 100, 146, 184, 185, 186, 219, 373, 379, 525, 639, 640, 745, 799, 1189, 1230,
                1430, 1432, 1533, 1538, 1547, 1723, 1844, 1934]

        deduped_cols = set(two_cols + three_cols + four_cols + five_cols) # can expand to include multiple lists
        return ["VAR_%04d"% col for col in deduped_cols]

    def requires(self):
        return RawData(cfg.train_data)

    def output(self):
        out_fname = "/".join([cfg.out_path, "categorical_encoders.p"])
        return luigi.LocalTarget(out_fname)

    def get_encode_df(self):
        """ Lame -- combine test and train unique values so encoder
        doesnt barf

        """
        data = pd.read_csv(self.input().path)
        gc.collect()
        data = data[self.get_powerful_cols()]
        gc.collect()
        data = data.fillna(-123)
        unique_vals = data.apply(lambda x: list(set(x)))

        return pd.Series([LabelEncoder().fit(row)  for row in unique_vals], index=data.columns)
            
    def run(self):
        encoders = self.get_encode_df()
        pickle.dump(encoders, open(self.output().path, 'wb'))

class OneHotEncode(luigi.Task):

    fname = luigi.Parameter(default="")
    
    def output(self):
        bname = basename(self.input().path)
        suffix = "onehot"
        new_name = "_".join([bname, suffix]) + ".csv"
        out_fname = "/".join([cfg.out_path, new_name])
        return luigi.LocalTarget(out_fname)
    

    def run(self):
        arg_dict = {"input": self.input().path, "output": self.output().path}
        subprocess.call("Rscript one_hot.R {input} {output}".format(**arg_dict), shell=True)

class EncodeDataset(luigi.Task):

    def requires(self):
        """ Two inputs required -- dataset and encoders for dataset
        order RawData(), Encoders()
        """
        pass

    def load_data(self):
        return pd.read_csv(self.input()[0].path)

    def load_encoders(self):
        return pickle.load(open(self.input()[1].path, 'rb'))

    def apply_col_encoders(self, data, encoders):
        col_encoder = encoders.ix[data.name]
        data = data.map(lambda x: random.choice(col_encoder.classes_) if x not in col_encoder.classes_ else x)
        encoded_col = col_encoder.transform(data.values)
        return encoded_col

    def transform(self):
        data = self.load_data()
        encoders = self.load_encoders()
        data = data[encoders.index]
        gc.collect()
        data = data.fillna(-123)
        return data.apply(lambda x: self.apply_col_encoders(x, encoders), axis=0)

    def run(self):
        encoded_data = self.transform()
        encoded_data.to_csv(self.output().path, index=False)
        gc.collect()



if __name__ == "__main__":
    luigi.run()
