cd ~/documents/repos/springleaf

datadir=/Users/aaronpolhamus/Dropbox/Polhooper/LeadScore/springleaf/data/devdata

Rscript converters/file_split.R  ${datadir}/test_val.csv ${datadir}/traindev.csv ${datadir}/testdev.csv 0.8

python converters/pre_vw_formatter.py ${datadir}/traindev.csv ${datadir}/traindev_normed.csv
python converters/pre_vw_formatter.py ${datadir}/testdev.csv ${datadir}/testdev_normed.csv

cd ~/documents/repos/leadscore/leadscore
python3 springleaf_encode.py 

cd ~/documents/repos/springleaf

python multimodel.py /Users/aaronpolhamus/Dropbox/Polhooper/LeadScore/springleaf/data/devdata/traindev /Users/aaronpolhamus/Dropbox/Polhooper/LeadScore/springleaf/data/devdata/testdev /Users/aaronpolhamus/Documents/repos/springleaf/vw_tests /Users/aaronpolhamus/Dropbox/Polhooper/LeadScore/springleaf/outputs/vt_test_run 

python multimodel.py /Users/aaronpolhamus/Dropbox/Polhooper/LeadScore/springleaf/data/devdata/traindev /Users/aaronpolhamus/Dropbox/Polhooper/LeadScore/springleaf/data/devdata/testdev /Users/aaronpolhamus/Documents/repos/springleaf/models_08_oct /Users/aaronpolhamus/Dropbox/Polhooper/LeadScore/springleaf/outputs/devrun_08oct15

#for master data
cd ~/documents/repos/springleaf

datadir=/Users/aaronpolhamus/Dropbox/Polhooper/LeadScore/springleaf/data/master_data

python converters/pre_vw_formatter.py ${datadir}/trainout.csv ${datadir}/trainout_normed.csv
python converters/pre_vw_formatter.py ${datadir}/testout.csv ${datadir}/testout_normed.csv

cd ~/documents/repos/leadscore/leadscore
python3 springleaf_encode.py 

cd ~/documents/repos/springleaf

python multimodel.py /Users/aaronpolhamus/Dropbox/Polhooper/LeadScore/springleaf/data/master_data/trainout /Users/aaronpolhamus/Dropbox/Polhooper/LeadScore/springleaf/data/master_data/testout /Users/aaronpolhamus/Documents/repos/springleaf/models_08_oct /Users/aaronpolhamus/Dropbox/Polhooper/LeadScore/springleaf/outputs/first_master_run 
