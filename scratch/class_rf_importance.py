#!/usr/bin/env python

import csv
from sys import argv
import pandas as pd 
from sklearn.ensemble import RandomForestClassifier 
from sklearn.externals import joblib 

input_data = argv[1]
outdir = argv[2]

print "training data: " + input_data

train = pd.read_csv(input_data)

classforest = RandomForestClassifier(
n_estimators = 500, 
max_features = 'sqrt', 
n_jobs = -1)

print "Training regression forest..."
X = train.ix[:, 1:train.shape[1]]
y = train['target']
classforest.fit(X = X, y = y) 

feature_names = X.columns.values

print "Features sorted by their score..."
imps = sorted(zip(map(lambda x: round(x, 4), classforest.feature_importances_), feature_names), reverse=True)

with open('%s/class_imp.csv' % outdir,'w') as out:
    csv_out=csv.writer(out)
    csv_out.writerow(['imp', 'var'])
    for row in imps:
        csv_out.writerow(row)
