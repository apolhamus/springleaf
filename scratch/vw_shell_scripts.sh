source activate lscore
cd ~/Documents/repos/leadscore/leadscore
python springleaf_encode.py #..note: this assumes that data has been harmonized and edits have been made to python source files 

head -1 testsep21_6pm.vw
head -1 trainsep21_6pm.vw

cd /Users/aaronpolhamus/Dropbox/Polhooper/LeadScore/springleaf/data/vw_prelim #..this is the dirctory that we put the transformed files in 

#-------------------------------------------------------------------------------
#..train models using internal holdout sample validation and score new data file
#-------------------------------------------------------------------------------

#..ALWAYS RUN THIS FIRST
rm nnet.cache lg.cache

#..log model
#-----------

traindata=trainsep22_1pm.vw
testdata=testsep22_1pm.vw
preds=lg_preds_2.txt
modelname=lg_2.vw
lrate=0.287092542426329
regrate=6.57790743265067e-06
npasses=25


vw -d ${traindata} --cache_file lg.cache -f ${modelname} --passes ${npasses} --loss_function logistic -l ${lrate} -q cb --l2 ${regrate}
vw -d ${testdata} -t -i ${modelname} --link=logistic -p ${preds}

# (vw-hypersearch)
# l2 
vw-hypersearch 1e-10  5e-4 vw ${traindata} --cache_file lg.cache -f ${modelname} --passes ${npasses} --loss_function logistic -l ${lrate} -q cb --l2 % 
vw-hypersearch 0.001  0.75 vw ${traindata} --cache_file lg.cache -f ${modelname} --passes ${npasses} --loss_function logistic -q cb -l % 


#..nnet model

traindata=trainsep22_1pm.vw
testdata=testsep22_1pm.vw
preds=nn_preds_2.txt
modelname=nn_2.vw
lrate=0.5
npasses=20
layers=20

vw -d ${traindata} --cache_file nnet.cache --nn ${layers} -f ${modelname} --passes ${npasses} -l ${lrate} --loss_function logistic -q cb
vw -d ${testdata} -t -i ${modelname} --link=logistic -p ${preds}

#----------------------------------------------------------------------------------#
#..Use hypersearch to search for locally optimal values of selected VW parameters  #
#----------------------------------------------------------------------------------#

#..log model 


#..neural net model 
