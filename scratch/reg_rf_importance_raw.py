#!/usr/bin/env python

import csv
from sys import argv
import pandas as pd 
from sklearn.ensemble import RandomForestRegressor 
from sklearn.externals import joblib 

input_data = argv[1]
outdir = argv[2]

train = pd.read_csv(input_data)

print "training data: " + input_data

regforest = RandomForestRegressor(
n_estimators = 500, 
max_features = 'sqrt', 
min_samples_leaf = 100, 
n_jobs = -1)

print "Training regression forest..."
X = train.ix[:, 1:train.shape[1]]
y = train['target']
regforest.fit(X = X, y = y) 

feature_names = X.columns.values

print "Features sorted by their score..."
imps = sorted(zip(map(lambda x: round(x, 4), regforest.feature_importances_), feature_names), reverse=True)

with open('%s/reg_imp.csv' % out_dir,'w') as out:
    csv_out=csv.writer(out)
    csv_out.writerow(['imp', 'var'])
    for row in imps:
        csv_out.writerow(row)
