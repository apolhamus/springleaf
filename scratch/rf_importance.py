from sys import argv
import pandas as pd 
from sklearn.ensemble import RandomForestClassifier 
from sklearn.externals import joblib 

model_dir = '/tmp/output'

regrf = joblib.load(model_dir + '/rf_reg.pkl') 
classrf = joblib.load(model_dir + '/rf_class.pkl') 
