#!/usr/bin/env python

import os 
import re 
import sys 

def main():
    
    data_dir = sys.argv[1]
    base_output_dir = sys.argv[2]
    model_dir = sys.argv[3]
    n_files = int(sys.argv[4])
    
    if not os.path.isdir(base_output_dir):
        os.makedirs(base_output_dir)
    else: 
        print "The directory %s already exists. Set a new one or delete the current directory." % base_output_dir
        sys.exit(1)
    
    data_files = os.listdir(data_dir)
    data_files = ['%s/%s' % (data_dir, fname) for fname in data_files]

    for i in range(0, n_files - 1):
        
        #..match all fils corresponding to the current iteration
        def Matcher(file_list, i, suffix):
            match_string = '[0-%d]\\.%s' % (i, suffix)     
            regex = re.compile(match_string)
            targets = filter(lambda i: regex.search(i), file_list)
            return(targets)

        csv_files = Matcher(data_files, i, 'csv')
        vw_files = Matcher(data_files, i, 'vw')

        #..combine all files corresponding to current iteration into a single training file 
        #  print to /tmp
        def Combiner(file_list, out_dir, suffix):
            fname = "%s/tmp_train.%s" % (out_dir, suffix) 
            with open(fname, 'w') as outfile:
                first_file = True
                for fname in file_list:
                    with open(fname) as infile:
                        count = 0 
                        for line in infile:
                            if not first_file and count == 0:
                                count += 1
                                next
                            else:  
                                outfile.write(line)
                                first_file = False
                                count += 1

        Combiner(csv_files, base_output_dir, 'csv')
        Combiner(vw_files, base_output_dir, 'vw')
        
        #..run multimodel script of n_files - 1 slices  
        traindata = "%s/tmp_train" % base_output_dir
        testdata = "%s/slice_%d" % (data_dir, n_files -1)
        out_dir = "%s/iteration_%d" % (base_output_dir, i)
        command_call = 'python multimodel.py %s %s %s %s' % (traindata, testdata, model_dir, out_dir)
        command_call = os.system(command_call)
     
if __name__  == "__main__":
    main()
