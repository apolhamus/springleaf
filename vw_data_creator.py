import sys, subprocess
import pandas as pd 
import numpy as np 
import os 

base_dir = '/home/ubuntu/prodrun/concept2'

for run in [1, 4]:
    for tag in ['a', 'b']:
        run_dir = '%s/run%d' % (base_dir, run)
        run_sub_dir = '%s/run%s' % (run_dir, tag) 
        
        print(run_sub_dir)
        
        if tag == 'a':
            data_file = '%s/slice_2.csv' % run_dir
        else:  
            data_file = '%s/slice_1.csv' % run_dir 
        
        print('scanning train...')
        df = pd.read_csv(data_file)
        xgb_preds = pd.read_csv('%s/xgb.csv' % run_sub_dir)
        rf_preds = np.loadtxt('%s/rf_preds_reg.txt' % run_sub_dir)
        
        df['xgb'] = list(xgb_preds['target'])
        df['rf'] = list(rf_preds)
        
        feature_names = [x for x in df.columns.values if x not in ['target', 'ID', 'xgb', 'rf']] 
        features_to_write = ['target', 'xgb', 'rf'] + feature_names
        df = df[features_to_write]
        
        msk = np.random.rand(len(df)) < 0.75
        train = df[msk]
        test = df[~msk]
        
        print('write vw files and run vw ensemblers')
        
        vw_dir = '%s/vw_files' % run_sub_dir
        if not os.path.exists(vw_dir):
            os.makedirs(vw_dir)
        
        train['target'] = [x if x == 1 else -1 for x in train['target']]
        test['target'] = [x if x == 1 else -1 for x in test['target']]
        
        #train.rename(columns={'target':'y'}, inplace=True)
        #test.rename(columns={'target':'y'}, inplace=True)
        
        train.to_csv('%s/train.csv' % vw_dir, headers = True, index = False)
        test.to_csv('%s/test.csv' % vw_dir, headers = True, index = False)
        
        cmd = 'python converters/pre_vw_formatter.py %s/train.csv %s/train.csv' % (vw_dir, vw_dir)       
        subprocess.call(cmd, shell=True) 
         
        cmd = 'python converters/pre_vw_formatter.py %s/test.csv %s/test.csv' % (vw_dir, vw_dir)       
        subprocess.call(cmd, shell=True) 
        
        cmd = 'python3 converters/springleaf_encode.py %s/train.csv' % vw_dir       
        subprocess.call(cmd, shell=True) 
        
        cmd = 'python3 converters/springleaf_encode.py %s/test.csv' % vw_dir       
        subprocess.call(cmd, shell=True) 
        
        cmd = 'rm -rf %s/out' % vw_dir
        subprocess.call(cmd, shell=True)
        
        cmd = 'python multimodel.py %s/train %s/test models/vw_models %s/out' % (vw_dir, vw_dir, vw_dir)
        subprocess.call(cmd, shell=True)
