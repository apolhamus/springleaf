def gen_feats(row, fieldnames):
	feats = []
	continuous_features = ['VAR_10']
	for var_name in fieldnames:
		value = row.get(var_name, '')

		if var_name in continuous_features:
			if value != '':
				value = value
			else:
				continue
		if var_name.startswith("PCA"):
			value = math.round(value, 1)

		if var_name.endswith('bin'):
			if value == 0:
				continue
		if var_name.startswith("GBDT"):
			#var_num = 20000 + var_name.split('_')[1] # grab the PCA number and assign to new order of mag
			#var_name = "_".join(["GBDT", var_num)
			pass

		key = var_name + ':' + str(value)
		feats.append(key)
	return feats


fieldnames = ['VAR_Xf_bin', 'VAR_10']
row = {'VAR_Xf_bin': 0, "VAR_10": 11}
row2 = {'VAR_Xf_bin': 1, 'VAR_10': ""}

print gen_feats(row, fieldnames)
print gen_feats(row2, fieldnames)
