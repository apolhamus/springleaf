import subprocess, sys, os

data_dir = '~/basedata'

print('CONCEPT 1:')

sys.stdout.flush()

if False:
    print('run 1')
    #0.794058
    cmd = 'Rscript xgboost.R %s/train.RData %s/test.RData ~/prodrun/concept1/best_run1 0 0 0 0.0075 8 0.75 0.75' % (data_dir, data_dir)
    subprocess.call(cmd, shell=True) 

    sys.stdout.flush()

    print('run 2')
    #.079433
    cmd = 'Rscript xgboost.R %s/train.RData %s/test.RData ~/prodrun/concept1/best_run2 0 0 0 0.0075 12 0.5 0.5' % (data_dir, data_dir)
    subprocess.call(cmd, shell=True) 

    sys.stdout.flush()

#if False: 
#    print('run 3')
#    #..789697 
#    cmd = 'Rscript xgboost.R %s/train.RData %s/test.RData ~/prodrun/concept1/best_run3 0 0 0 0.005 14 0.5 0.5' % (data_dir, data_dir)
#    subprocess.call(cmd, shell=True) 
#
#    sys.stdout.flush()
#    
#    print('run 4')
#    #..789961
#    cmd = 'Rscript xgboost.R %s/train.RData %s/test.RData ~/prodrun/concept1/best_run4 0 0 0 0.005 16 0.4 0.4' % (data_dir, data_dir)
#    subprocess.call(cmd, shell=True) 
#
#    sys.stdout.flush()

if True: 
    print('run 3')
    #..789697 
    cmd = 'Rscript xgboost.R %s/train.RData %s/test.RData ~/prodrun/concept1/best_run3 0 0 0 0.005 8 0.75 0.75' % (data_dir, data_dir)
    subprocess.call(cmd, shell=True) 

    sys.stdout.flush()
    
    print('run 4')
    #..789961
    cmd = 'Rscript xgboost.R %s/train.RData %s/test.RData ~/prodrun/concept1/best_run4 0 0 0 0.005 12 0.8 0.8' % (data_dir, data_dir)
    subprocess.call(cmd, shell=True) 

    sys.stdout.flush()
