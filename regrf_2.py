import sys, subprocess
import pandas as pd 
from sklearn.grid_search import GridSearchCV
from sklearn.cross_validation import train_test_split
from sklearn.ensemble import RandomForestRegressor 
from sklearn.externals import joblib 

import settings.common as cfg 

train_data = sys.argv[1]
test_data = sys.argv[2]
out_dir = sys.argv[3]
format_data_flag = int(sys.argv[4])
kill_bad_feats = int(sys.argv[5])
use_good_feats = int(sys.argv[6])
n_estimators = int(sys.argv[7]) #500, 
max_features = int(sys.argv[8]) #300, 
min_samples_leaf = int(sys.argv[9]) #10, 

if(format_data_flag == 1): 
    print('formatting and scanning in data files...')
    cmd = 'Rscript core_data_prep.R'
    subprocess.call(cmd, shell=True) 
elif(format_data_flag == 0): 
    print('scanning in data files...')
else: 
    print('Set the first argument, format_data_flag equal to 0 or 1')
    sys.exit(1)

train = pd.read_csv(train_data)
test = pd.read_csv(test_data)

if(kill_bad_feats == 1): 
    #bad_feats = [x.strip() for x in open('ancillary_files/features_to_dump.csv', 'rb').readlines()]
    bad_feats = open('ancillary_files/features_to_dump.csv', 'rb').readlines()[0].split('\r')
    train.drop(labels=bad_feats, axis=1, inplace=True)     
    test.drop(labels=bad_feats, axis=1, inplace=True)

if(use_good_feats == 1):
    good_feats = open('ffm/ancillary_files/ffm_vars.csv', 'rb').readlines()[0].split('\r')
    
    good_feats = good_feats + ['ID', 'target']
    train = train[good_feats] 
    test['target'] = 0    
    test = test[good_feats]

regforest = RandomForestRegressor(
n_estimators = n_estimators, 
max_features = max_features, 
min_samples_leaf = min_samples_leaf, 
n_jobs = -1)

feature_names = [x for x in train.columns.values if not x in ['target', 'ID']]

print "Training regression forest..."
regfit = regforest.fit(X = train[feature_names], y = train['target']) 

print "Generating predictions..."
preds_test = regfit.predict(test[feature_names])

print "Writing predictions to test file..."
def WriteFunction(outdir, preds, fname):
    with open('%s/%s' % (outdir, fname), 'wb') as writer: 
        for item in preds:
            writer.write("%s\n" % item )

WriteFunction(out_dir, preds_test, 'rf_preds_reg.txt')

print "Saving regression forest to %s..." % out_dir 
joblib.dump(regfit, '%s/rf_reg.pkl' % out_dir, compress=9)

#..interactive script for grid searching parameter space: 
if False: 
    X = train[feature_names]
    y = train['target']
    
    param_grid = [{'min_samples_leaf':[2, 5, 10], 'max_features':[150, 300, 600]}]
    #param_grid = [{'min_samples_leaf':[10, 100, 500, 2500, 10000], 'max_features':['sqrt', 'log2']}, {'min_samples_leaf':[10, 100, 500, 2500, 10000], 'max_features':[75, 150]}]
    #param_grid = [{'min_samples_leaf':[10, 50], 'max_features':[0.1, 0.25]}]
    clf = GridSearchCV(regfit, param_grid, cv = 3) #..this should be 'roc_auc' fot classification forest
    clf.fit(X, y)
    joblib.dump(clf, '%s/rf_clf_reg.pkl' % out_dir, compress=9)
    
    #Valid scoring options are ['accuracy', 'adjusted_rand_score', 'average_precision', 'f1', 'log_loss', 'mean_squared_error', 'precision', 'r2', 'recall', 'roc_auc']
