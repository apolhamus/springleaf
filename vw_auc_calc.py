import sys, subprocess
import pandas as pd 
import numpy as np 

base_dir = '/home/ubuntu/prodrun/concept2'

for run in [1, 4]:
    for tag in ['a', 'b']:
        
        print(run_sub_dir)
        
        run_dir = '%s/run%d' % (base_dir, run)
        run_sub_dir = '%s/run%s' % (run_dir, tag) 
        vw_sub_dir= '%s/vw_files/out' % run_sub_dir

        lg = pd.Series(np.loadtxt('%s/lg_vw_preds_test.txt' % vw_sub_dir), name = 'lg')
        nn = pd.Series(np.loadtxt('%s/nn_vw_preds_test.txt' % vw_sub_dir), name = 'nn')
        df = pd.read_csv('%s/vw_files/test.csv' % run_sub_dir)
        target = pd.Series([x if x == 1 else 0 for x in df['y']], name = 'target')
        xgb = df['xgb']
        rf = df['rf']

        outdf = pd.concat([target, lg, nn, xgb, rf], axis = 1)
        outdf.to_csv('%s/val_df.csv' % vw_sub_dir, headers = True, index = False)

        cmd = 'Rscript scratch/auc_printer_2.R %s' % vw_sub_dir
        subprocess.call(cmd, shell=True)
