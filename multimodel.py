#!/usr/bin/env python
import os 
import re 
import sys

def main():
    """
    Trains models using scikit-learn, vw, and R.  All models are coded to be executed from the 
    command line and are stored as script files in the a mmodel_directory in the repo. 
    This model directory defines defines the ensemble. Every script added to the ensemble
    should accept three command line arguments that this script will pass in: 
    (1) a test file (csv or vw), 
    (2) a train file (csv or vw)
    (3) a directory of command line-implemented model script files. the directory defines the ensemble
    (4) an output directory
    
    NOTE: the traindata/testdata arguments should be full file names with paths, ONLY missing the .csv/.vw extension. 
          .csv and .vw files with identical prefixes should be present in the directory used for training and test data, e.g.:
          /Users/aaronpolhamus/Dropbox/Polhooper/LeadScore/springleaf/data/devdata_thirds/slice_2.csv
          /Users/aaronpolhamus/Dropbox/Polhooper/LeadScore/springleaf/data/devdata_thirds/slice_2.vw

    If no vw scripts are present in the ensemble then this condition doesnt matter. 
    
    All matrixes should be numeric and one-hot encoded. 
    
    Here's an example call: 
    
    python multimodel.py /Users/aaronpolhamus/Dropbox/Polhooper/LeadScore/springleaf/data/devdata_thirds/slice_0 /Users/aaronpolhamus/Dropbox/Polhooper/LeadScore/springleaf/data/devdata_thirds/slice_1 models /tmp/one_more_test_run_2
    """ 
    
    traindata = sys.argv[1]
    testdata  = sys.argv[2]
    model_directory = sys.argv[3]
    output_directory = sys.argv[4]
    
    train_file_csv = traindata + '.csv'
    test_file_csv  = testdata + '.csv'
    
    train_file_vw = traindata + '.vw'
    test_file_vw  = testdata + '.vw'
            
    if not os.path.isdir(output_directory):
        os.makedirs(output_directory)
    else: 
        print "The directory %s already exists. Set a new one or delete the current directory." % output_directory
        sys.exit(1)
    
    modeldir =  os.path.join(os.getcwd(), model_directory) 
    modelscripts = os.listdir(modeldir)
    
    #Sequential execution of all models
    for model in modelscripts:
        if(re.search('\\.py', model)):
            cmd_line_call = 'cd %s;python %s %s %s %s' % (modeldir, model, train_file_csv, test_file_csv, output_directory)
            os.system(cmd_line_call)
        elif(re.search('\\.R', model)):
            cmd_line_call = 'cd %s;Rscript %s %s %s %s' % (modeldir, model, train_file_csv, test_file_csv, output_directory)
            os.system(cmd_line_call)
        elif(re.search('\\.sh', model)):
            os.environ['PATH'] += ":" + os.getcwd()
            cmd_line_call = "cd %s;chmod ug+x %s;./%s %s %s %s" % (modeldir, model, model, train_file_vw, test_file_vw, output_directory)
            os.system(cmd_line_call)
        else: 
            print "'%s' was not a model" % model 

if __name__  == "__main__":
    main()
