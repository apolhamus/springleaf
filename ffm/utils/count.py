#!/usr/bin/env python3

import argparse
import csv
import sys
import collections

if len(sys.argv) == 1:
    sys.argv.append('-h')

parser = argparse.ArgumentParser()
parser.add_argument('csv_path', type=str)
args = vars(parser.parse_args())


counts = collections.defaultdict(lambda : [0, 0, 0])

def var_scanner():    
    with open('ancillary_files/cat_feats.csv', 'r') as csvfile:
        reader = csv.reader(csvfile)
        feats = []
        for row in reader:
            entry = row[0] 
            if not entry == 'x': 
                feats.append(entry)
    
    return feats
    
cat_vars = var_scanner()

for i, row in enumerate(csv.DictReader(open(args['csv_path'])), start=1):
    target = row['target']
    for cat_var in cat_vars:
        value = row[cat_var]
        if target == '0':
            counts[cat_var+','+value][0] += 1
        else:
            counts[cat_var+','+value][1] += 1
        counts[cat_var+','+value][2] += 1
    if i % 1000000 == 0:
        sys.stderr.write('{0}m\n'.format(int(i/1000000)))

print('Field,Value,Neg,Pos,Total,Ratio')
for key, (neg, pos, total) in sorted(counts.items(), key=lambda x: x[1][2]):
    if total < 10:
        continue
    ratio = round(float(pos)/total, 5)
    print(key+','+str(neg)+','+str(pos)+','+str(total)+','+str(ratio))
