#!/usr/bin/env python3

import subprocess, sys, os, time

NR_THREAD = 16 

start = time.time()

cmd = 'converters/parallelizer-b.py -s {nr_thread} converters/pre-b.py tr.csv tr.sp'.format(nr_thread=NR_THREAD)
subprocess.call(cmd, shell=True) 

cmd = 'converters/parallelizer-b.py -s {nr_thread} converters/pre-b.py te.csv te.sp'.format(nr_thread=NR_THREAD)
subprocess.call(cmd, shell=True) 

cmd = './ffm -k 8 -t 10 -s {nr_thread} te.sp tr.sp'.format(nr_thread=NR_THREAD) 
subprocess.call(cmd, shell=True)

cmd = './utils/calibrate.py te.sp.out te.sp.out.cal'.format(nr_thread=NR_THREAD) 
subprocess.call(cmd, shell=True)

cmd = './utils/make_submission.py te.sp.out.cal submission.csv'.format(nr_thread=NR_THREAD) 
subprocess.call(cmd, shell=True)

print('time used = {0:.0f}'.format(time.time()-start))
