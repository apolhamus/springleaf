import hashlib
import csv
import math
import os
import subprocess

def VarScanner(file_path):    
    with open(file_path, 'rU') as csvfile:
        reader = csv.reader(csvfile)
        feats = []
        for row in reader:
            entry = row[0] 
            if not entry == 'x': 
                feats.append(entry)
    
    return(feats)

def open_with_first_line_skipped(path, skip=True):
    f = open(path)
    if not skip:
        return f
    next(f)
    return f

def hashstr(str, nr_bins):
    return int(hashlib.md5(str.encode('utf8')).hexdigest(), 16)%(nr_bins-1)+1

def log_transform(value):
	value = float(value)
	if value > 2:
		value = int(math.log(float(value))**2)
	else:
		value = 'SP'+str(value)
	return value

def gen_feats(row, fieldnames):
    feats = []
    continuous_features = [x.strip() for x in open("ancillary_files/continuous_variables.csv").readlines()]
    my_continuous = [x.strip() for x in open("ancillary_files/gb_vars.csv").readlines()]
    best_feats = ['age', 'lead_quality'] + my_continuous
    best_feats = [x.strip() for x in open("ancillary_files/ffm_vars.csv").readlines()]
    best_feats = best_feats + [x for x in fieldnames if x.startswith('GBDT')]
    best_feats = best_feats + [x for x in fieldnames if x.endswith('bin')]
    best_feats = best_feats + [x for x in fieldnames if x.endswith('ts')] 
    best_feats = best_feats + [x for x in fieldnames if x.endswith('ne')]

    for var_name in best_feats:
        value = row.get(var_name, '') 

        if var_name in continuous_features:
            if value != '': 
                value = value
        elif var_name in my_continuous:
            if value != '':
                if value > 0:
                    value = 1
        elif var_name.startswith("PCA"):
            value = round(float(value), 1)
        elif var_name.endswith('bin'):
            if value == 0:
                continue
        elif var_name.startswith("GBDT"):
            value = value
        else:
            continue # if not in above list -- pass over
        key = var_name + ':' + str(value)
        feats.append(key)
    return feats
    

def read_freqent_feats(threshold=10):
    frequent_feats = set()
    for row in csv.DictReader(open('fc.trva.t10.txt')):
        if int(row['Total']) < threshold:
            continue
        frequent_feats.add(row['Field']+'-'+row['Value'])
    return frequent_feats

def split(path, nr_thread, has_header):

    def open_with_header_witten(path, idx, header):
        f = open(path+'.__tmp__.{0}'.format(idx), 'w')
        if not has_header:
            return f 
        f.write(header)
        return f

    def calc_nr_lines_per_thread():
        nr_lines = int(list(subprocess.Popen('wc -l {0}'.format(path), shell=True, 
            stdout=subprocess.PIPE).stdout)[0].split()[0])
        if not has_header:
            nr_lines += 1 
        return math.ceil(float(nr_lines)/nr_thread)

    header = open(path).readline()

    nr_lines_per_thread = calc_nr_lines_per_thread()

    idx = 0
    f = open_with_header_witten(path, idx, header)
    for i, line in enumerate(open_with_first_line_skipped(path, has_header), start=1):
        if i%nr_lines_per_thread == 0:
            f.close()
            idx += 1
            f = open_with_header_witten(path, idx, header)
        f.write(line)
    f.close()

def parallel_convert(cvt_path, arg_paths, nr_thread):

    workers = []
    for i in range(nr_thread):
        cmd = '{0}'.format(os.path.join('.', cvt_path))
        for path in arg_paths:
            cmd += ' {0}'.format(path+'.__tmp__.{0}'.format(i))
        worker = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
        workers.append(worker)
    for worker in workers:
        worker.communicate()

def cat(path, nr_thread):
    
    if os.path.exists(path):
        os.remove(path)
    for i in range(nr_thread):
        cmd = 'cat {svm}.__tmp__.{idx} >> {svm}'.format(svm=path, idx=i)
        p = subprocess.Popen(cmd, shell=True)
        p.communicate()

def delete(path, nr_thread):
    
    for i in range(nr_thread):
        os.remove('{0}.__tmp__.{1}'.format(path, i))

