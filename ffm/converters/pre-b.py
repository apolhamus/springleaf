#!/usr/bin/env python3

import argparse, csv, sys

from common import *

if len(sys.argv) == 1:
    sys.argv.append('-h')

from common import *

parser = argparse.ArgumentParser()
parser.add_argument('-n', '--nr_bins', type=int, default=int(1e+6))
parser.add_argument('-t', '--threshold', type=int, default=int(10))
parser.add_argument('csv_path', type=str)
parser.add_argument('out_path', type=str)
args = vars(parser.parse_args())

def gen_hashed_fm_feats(feats, nr_bins):
    """ Hash string of one hot encode variable

    Example:
    Column VAR1 has value 10. This will has VAR1-10
    Returning a 
    """
    feats = [(field, hashstr(feat, nr_bins)) for (field, feat) in feats]
    feats = ['{0}'.format(idx) for (field, idx) in feats]
    return feats


with open(args['out_path'], 'w') as f:
    reader = csv.DictReader(open(args['csv_path']))
    fieldnames = reader.fieldnames
    for row in reader:
        feats = []
        for feat in gen_feats(row, fieldnames):
            var_num = feat.split(':')[0]
            feats.append((var_num, feat))

        feats = gen_hashed_fm_feats(feats, args['nr_bins'])
        f.write(row['target'] + ' ' + ' '.join(feats) + '\n')
