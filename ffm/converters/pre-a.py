#!/usr/bin/env python

import csv
import sys

import common

if len(sys.argv) == 1:
    sys.argv.append('-h')

csv_path = sys.argv[1]
dense_path = sys.argv[2]
sparse_path = sys.argv[3]

#..scan in supplemental data files that will define feature combinations for sparse and 


cat_features = common.VarScanner('ancillary_files/cat_feats.csv')
dens_feats = common.VarScanner('ancillary_files/dense_feats.csv')

target_cat_feats = ['VAR_0505:0', 'VAR_0505:1', 'VAR_0138:0', 'VAR_0106:0', 'VAR_0362:0', 'VAR_0362:1', 'VAR_0339:-1', 'VAR_0339:0', 'VAR_0339:1', 'VAR_0275:0', 'VAR_0545:0', 'VAR_1816:1', 'VAR_0104:0', 'VAR_1770:0', 'VAR_0105:0', 'VAR_0120:0', 'VAR_0121:0', 'VAR_0144:0', 'VAR_1794:0', 'VAR_1791:0', 'VAR_0145:0', 'VAR_0017:0', 'VAR_0856:-9999', 'VAR_0855:-9999', 'VAR_1391:9998', 'VAR_0556:9996', 'VAR_0854:-9999', 'VAR_0062:1', 'VAR_1124:0', 'VAR_0068:0', 'VAR_0884:-9999', 'VAR_0069:0', 'VAR_0086:1', 'VAR_0005:C', 'VAR_0005:B', 'VAR_0005:N', 'VAR_0005:S', 'VAR_0466:-1', 'VAR_0466:I', 'VAR_0466:', 'VAR_0232:true', 'VAR_0232:false', 'VAR_0232:', 'VAR_0001:H', 'VAR_0001:R', 'VAR_0001:Q', 'VAR_0237:FL', 'VAR_0237:CA', 'VAR_0237:WV', 'VAR_0237:TX', 'VAR_0237:IL', 'VAR_0237:OR', 'VAR_0237:GA', 'VAR_0237:PA', 'VAR_0237:TN', 'VAR_0237:WA', 'VAR_0237:VA', 'VAR_0237:KY', 'VAR_0237:IN', 'VAR_0237:MO', 'VAR_0237:AL', 'VAR_0237:OH', 'VAR_0237:SC', 'VAR_0237:LA', 'VAR_0237:NV', 'VAR_0237:NC', 'VAR_0237:CO', 'VAR_0237:NY', 'VAR_0237:OK', 'VAR_0237:WI', 'VAR_0237:MD', 'VAR_0237:HI', 'VAR_0237:NM', 'VAR_0237:MS', 'VAR_0237:AZ', 'VAR_0237:WY', 'VAR_0237:ID', 'VAR_0237:MI', 'VAR_0237:NJ', 'VAR_0237:KS', 'VAR_0237:IA', 'VAR_0237:UT', 'VAR_0237:NE', 'VAR_0237:MT', 'VAR_0237:SD', 'VAR_0237:DE', 'VAR_0237:DC', 'VAR_0237:AR', 'VAR_0237:', 'VAR_0237:MN', 'VAR_0237:CT', 'VAR_0237:AK', 'VAR_0325:-1', 'VAR_0325:H', 'VAR_0325:R', 'VAR_0325:S', 'VAR_0325:P', 'VAR_0325:', 'VAR_0325:F', 'VAR_0325:M', 'VAR_0325:G', 'VAR_0325:U', 'VAR_0305:S', 'VAR_0305:P', 'VAR_0305:H', 'VAR_0305:-1', 'VAR_0305:', 'VAR_0305:R', 'VAR_0305:U', 'VAR_0305:M', 'VAR_0305:F', 'VAR_0283:S', 'VAR_0283:H', 'VAR_0283:-1', 'VAR_0283:P', 'VAR_0283:', 'VAR_0283:R', 'VAR_0283:F', 'VAR_0283:U', 'VAR_1934:IAPS', 'VAR_1934:RCC', 'VAR_1934:BRANCH', 'VAR_1934:MOBILE', 'VAR_1934:CSC']

def MissingElements(list_1, list_2, ok_to_keep = 'target'):
    out_list = []
    for element in list_1: 
        if not element in list_2 and not element in ok_to_keep: 
            out_list.append(element)
    
    return(out_list)

with open(dense_path, 'w') as f_d, open(sparse_path, 'w') as f_s:
    count = 0
    for row in csv.DictReader(open(csv_path)):
        
        feats = []
        
        if(count == 0):
            missing_dens_feats = MissingElements(dens_feats, row.keys())
            missing_cat_feats = MissingElements(cat_features, row.keys())
            # TODO: what are dens feats? Do you mean continuous?
            dens_feats = [x for x in dens_feats if x not in missing_dens_feats]
            cat_features = [x for x in cat_features if x not in missing_cat_feats]
            
            if(len(missing_dens_feats) > 0): 
                print('The continuous fields %s were listed as dense features but not present in the passed-in file. ' % str(missing_dens_feats))
            
            if(len(missing_cat_feats) > 0):
                print('The categorical fields %s were listed as dense features but not present in the passed-in file. ' % str(missing_cat_feats))
        
        for var in dens_feats:
            val = row[var]
            if val == '':
                # TODO: is this supposed to be -1?
                val = 1 #..TODO FOR NOW PICK A GOOD GENERIC FILL VALUE (-1). Missing data prevalence in key features only 0.006. 
            feats.append('{0}'.format(val))
        
        f_d.write(row['target'] + ' ' + ' '.join(feats) + '\n')
                
        if(count >= 5 and count < 10):
            if(count == 5): 
                print('dense header:') 
            print(row['target'] + ' ' + ' '.join(feats))
        
        cat_feats = set()
        for var in cat_features:
            key = var + ':' + row[var]
            cat_feats.add(key)
                
        feats = []
        for j, feat in enumerate(target_cat_feats, start=1):
            if feat in cat_feats:
                feats.append(str(j))
        f_s.write(row['target'] + ' ' + ' '.join(feats) + '\n')
        if(count < 5): 
            if(count == 0): 
                print('sparse header:')
            print(row['target'] + ' ' + ' '.join(feats))
        count += 1 
