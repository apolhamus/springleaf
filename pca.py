#!/usr/bin/env python
import pickle
import sys

import pandas as pd
from sklearn.preprocessing import Imputer
from sklearn.decomposition import PCA
import numpy as np

import preprocessing

def scale_function(array):
    result = [(x - np.mean(x))/(np.max(x) - np.min(x)) for x in array]
    return(np.array(result))

print('scanning in data files')

full_traindata = pd.read_csv(sys.argv[1])
indata = pd.read_csv(sys.argv[2])
outdata = sys.argv[3]
comps_only = int(sys.argv[4])

print('replace fill values')
to_replace = preprocessing.missing_value_generator()
full_traindata = full_traindata.replace(value=pd.np.nan, to_replace=to_replace)
full_traindata = full_traindata.convert_objects(convert_numeric=True)
indata = indata.replace(value=pd.np.nan, to_replace=to_replace)
indata = indata.convert_objects(convert_numeric=True)

print('select important variables')
n_vars = 500
imp_vars = pd.read_csv('ancillary_files/importance_data.csv')
imp_vars['mean_rank'] = imp_vars[['reg_imp_rank', 'Gain_gbm_rank', 'class_imp_rank']].mean(axis=1)
imp_vars.sort(['mean_rank'], inplace=True)
subset_vars = imp_vars['var'].tail(n_vars).values.tolist()

print('impute missing values')
full_traindata = full_traindata[subset_vars]
imputer = Imputer(missing_values=pd.np.nan, strategy='mean', axis=0)
full_traindata = imputer.fit_transform(full_traindata)

indata = indata[subset_vars]
imputer = Imputer(missing_values=pd.np.nan, strategy='mean', axis=0)
indata = imputer.fit_transform(indata)

print('Scale the data files')
full_traindata = scale_function(full_traindata)
indata = scale_function(indata)

print('fit PCA')
try: 
    pca =  pickle.load(open('/tmp/pca_encoder.p', 'rb'))
    print("##############\n")
    print("Loaded pca encoder from /tmp -- are you super sure that its the right encoder?\n")
    print("##############\n")
    
except:
    pca = PCA(n_components=30, whiten=True)
    pca.fit(full_traindata)
    
    pickle.dump(pca, open('/tmp/pca_encoder.p', 'wb')) # This will surely fuck us over before the competition ends --- (AP) Possibly. I'm putting rm statements in to kill the encoder before each run

components = pca.transform(indata)
components = pd.DataFrame(components, columns=["PCA_%0d" % x for x in range(pca.n_components)])

if(comps_only == 0):
    df = pd.concat([pd.read_csv(outdata), components], axis=1)
    df.to_csv(outdata, index=False)
elif(comps_only == 1): 
    components.to_csv(outdata, index = False)
else: 
    print("comps_only (fourth argument) must take a value of either 0 (no) or 1 (yes)")
    sys.exit(1)
