import subprocess, sys, os

data_dir = '~/basedata'

#-----------------
#..Concept 1 runs: 
#-----------------

print('CONCEPT 1:')

sys.stdout.flush()

print('run 1')
cmd = 'Rscript xgboost.R %s/train.RData %s/test.RData ~/prodrun/concept1/run1 0 0 0 0.01 8 0.75 0.75' % (data_dir, data_dir)
subprocess.call(cmd, shell=True) 
cmd = 'python regrf_2.py %s/train.csv %s/test.csv ~/prodrun/concept1/run1 0 0 0 500 40 10' % (data_dir, data_dir)
subprocess.call(cmd, shell=True) 

sys.stdout.flush()

print('run 2')
cmd = 'Rscript xgboost.R %s/train.RData %s/test.RData ~/prodrun/concept1/run2 0 0 0 0.01 10 0.5 0.5' % (data_dir, data_dir)
subprocess.call(cmd, shell=True) 
cmd = 'python regrf_2.py %s/train.csv %s/test.csv ~/prodrun/concept1/run2 0 0 0 500 100 50' % (data_dir, data_dir)
subprocess.call(cmd, shell=True) 

sys.stdout.flush()

print('run 3')
cmd = 'Rscript xgboost.R %s/train.RData %s/test.RData ~/prodrun/concept1/run3 0 0 0 0.01 10 0.5 0.5' % (data_dir, data_dir)
subprocess.call(cmd, shell=True) 
cmd = 'python regrf_2.py %s/train.csv %s/test.csv ~/prodrun/concept1/run3 0 0 0 500 300 100' % (data_dir, data_dir)
subprocess.call(cmd, shell=True) 

sys.stdout.flush()

print('run 4')
cmd = 'Rscript xgboost.R %s/train.RData %s/test.RData ~/prodrun/concept1/run4 0 0 1 0.01 8 0.75 0.75' % (data_dir, data_dir)
subprocess.call(cmd, shell=True) 
cmd = 'python regrf_2.py %s/train.csv %s/test.csv ~/prodrun/concept1/run4 0 0 1 500 40 10' % (data_dir, data_dir)
subprocess.call(cmd, shell=True) 

sys.stdout.flush()

print('run 5')
cmd = 'Rscript xgboost.R %s/train.RData %s/test.RData ~/prodrun/concept1/run5 0 0 1 0.01 10 0.5 0.5' % (data_dir, data_dir)
subprocess.call(cmd, shell=True) 
cmd = 'python regrf_2.py %s/train.csv %s/test.csv ~/prodrun/concept1/run5 0 0 1 500 100 50' % (data_dir, data_dir)
subprocess.call(cmd, shell=True) 

sys.stdout.flush()

print('run 6')
cmd = 'Rscript xgboost.R %s/train.RData %s/test.RData ~/prodrun/concept1/run6 0 0 1 0.01 10 0.5 0.5' % (data_dir, data_dir)
subprocess.call(cmd, shell=True) 
cmd = 'python regrf_2.py %s/train.csv %s/test.csv ~/prodrun/concept1/run6 0 0 1 500 300 100' % (data_dir, data_dir)
subprocess.call(cmd, shell=True) 

sys.stdout.flush()

#-----------------
#..Concept 2 runs: 
#-----------------

print('CONCEPT 2:')

print('run 1')
out_path = '~/prodrun/concept2/run1'
cmd = 'Rscript file_split.R %s/train.csv %s 0.5_0.5' % (data_dir, out_path)
subprocess.call(cmd, shell=True) 
cmd = 'Rscript xgboost.R %s/slice_1.RData %s/slice_2.RData %s/runa 0 0 0 0.01 8 0.75 0.75' % (out_path, out_path, out_path)
subprocess.call(cmd, shell=True) 
cmd = 'python regrf_2.py %s/slice_1.csv %s/slice_2.csv %s/runa 0 0 0 500 40 10' % (out_path, out_path, out_path)
subprocess.call(cmd, shell=True) 
cmd = 'Rscript xgboost.R %s/slice_2.RData %s/slice_1.RData %s/runb 0 0 0 0.01 8 0.75 0.75' % (out_path, out_path, out_path)
subprocess.call(cmd, shell=True) 
cmd = 'python regrf_2.py %s/slice_2.csv %s/slice_1.csv %s/runb 0 0 0 500 40 10' % (out_path, out_path, out_path)
subprocess.call(cmd, shell=True) 

sys.stdout.flush()

print('run 2')
out_path = '~/prodrun/concept2/run2'
cmd = 'Rscript file_split.R %s/train.csv %s 0.5_0.5' % (data_dir, out_path)
subprocess.call(cmd, shell=True) 
cmd = 'Rscript xgboost.R %s/slice_1.RData %s/slice_2.RData %s/runa 0 0 0 0.01 10 0.5 0.5' % (out_path, out_path, out_path)
subprocess.call(cmd, shell=True) 
cmd = 'python regrf_2.py %s/slice_1.csv %s/slice_2.csv %s/runa 0 0 0 500 100 50' % (out_path, out_path, out_path)
subprocess.call(cmd, shell=True) 
cmd = 'Rscript xgboost.R %s/slice_2.RData %s/slice_1.RData %s/runb 0 0 0 0.01 10 0.5 0.5' % (out_path, out_path, out_path)
subprocess.call(cmd, shell=True) 
cmd = 'python regrf_2.py %s/slice_2.csv %s/slice_1.csv %s/runb 0 0 0 500 100 50' % (out_path, out_path, out_path)
subprocess.call(cmd, shell=True) 

sys.stdout.flush()

print('run 3')
out_path = '~/prodrun/concept2/run3'
cmd = 'Rscript file_split.R %s/train.csv %s 0.5_0.5' % (data_dir, out_path)
subprocess.call(cmd, shell=True) 
cmd = 'Rscript xgboost.R %s/slice_1.RData %s/slice_2.RData %s/runa 0 0 0 0.01 12 0.4 0.4' % (out_path, out_path, out_path)
subprocess.call(cmd, shell=True) 
cmd = 'python regrf_2.py %s/slice_1.csv %s/slice_2.csv %s/runa 0 0 0 500 300 100' % (out_path, out_path, out_path)
subprocess.call(cmd, shell=True) 
cmd = 'Rscript xgboost.R %s/slice_2.RData %s/slice_1.RData %s/runb 0 0 0 0.01 12 0.4 0.4' % (out_path, out_path, out_path)
subprocess.call(cmd, shell=True) 
cmd = 'python regrf_2.py %s/slice_2.csv %s/slice_1.csv %s/runb 0 0 0 500 300 100' % (out_path, out_path, out_path)
subprocess.call(cmd, shell=True) 

sys.stdout.flush()

print('run 4')
out_path = '~/prodrun/concept2/run4'
cmd = 'Rscript file_split.R %s/train.csv %s 0.5_0.5' % (data_dir, out_path)
subprocess.call(cmd, shell=True) 
cmd = 'Rscript xgboost.R %s/slice_1.RData %s/slice_2.RData %s/runa 0 0 1 0.01 8 0.75 0.75' % (out_path, out_path, out_path)
subprocess.call(cmd, shell=True) 
cmd = 'python regrf_2.py %s/slice_1.csv %s/slice_2.csv %s/runa 0 0 1 500 40 10' % (out_path, out_path, out_path)
subprocess.call(cmd, shell=True) 
cmd = 'Rscript xgboost.R %s/slice_2.RData %s/slice_1.RData %s/runb 0 0 1 0.01 8 0.75 0.75' % (out_path, out_path, out_path)
subprocess.call(cmd, shell=True) 
cmd = 'python regrf_2.py %s/slice_2.csv %s/slice_1.csv %s/runb 0 0 1 500 40 10' % (out_path, out_path, out_path)
subprocess.call(cmd, shell=True) 

sys.stdout.flush()

print('run 5')
out_path = '~/prodrun/concept2/run5'
cmd = 'Rscript file_split.R %s/train.csv %s 0.5_0.5' % (data_dir, out_path)
subprocess.call(cmd, shell=True) 
cmd = 'Rscript xgboost.R %s/slice_1.RData %s/slice_2.RData %s/runa 0 0 1 0.01 10 0.5 0.5' % (out_path, out_path, out_path)
subprocess.call(cmd, shell=True) 
cmd = 'python regrf_2.py %s/slice_1.csv %s/slice_2.csv %s/runa 0 0 1 500 100 50' % (out_path, out_path, out_path)
subprocess.call(cmd, shell=True) 
cmd = 'Rscript xgboost.R %s/slice_2.RData %s/slice_1.RData %s/runb 0 0 1 0.01 10 0.5 0.5' % (out_path, out_path, out_path)
subprocess.call(cmd, shell=True) 
cmd = 'python regrf_2.py %s/slice_2.csv %s/slice_1.csv %s/runb 0 0 1 500 100 50' % (out_path, out_path, out_path)
subprocess.call(cmd, shell=True) 

sys.stdout.flush()

print('run 6')
out_path = '~/prodrun/concept2/run6'
cmd = 'Rscript file_split.R %s/train.csv %s 0.5_0.5' % (data_dir, out_path)
subprocess.call(cmd, shell=True) 
cmd = 'Rscript xgboost.R %s/slice_1.RData %s/slice_2.RData %s/runa 0 0 1 0.01 12 0.4 0.4' % (out_path, out_path, out_path)
subprocess.call(cmd, shell=True) 
cmd = 'python regrf_2.py %s/slice_1.csv %s/slice_2.csv %s/runa 0 0 1 500 300 100' % (out_path, out_path, out_path)
subprocess.call(cmd, shell=True) 
cmd = 'Rscript xgboost.R %s/slice_2.RData %s/slice_1.RData %s/runb 0 0 1 0.01 12 0.4 0.4' % (out_path, out_path, out_path)
subprocess.call(cmd, shell=True) 
cmd = 'python regrf_2.py %s/slice_2.csv %s/slice_1.csv %s/runb 0 0 1 500 300 100' % (out_path, out_path, out_path)
subprocess.call(cmd, shell=True) 

sys.stdout.flush()
