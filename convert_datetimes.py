#!/usr/bin/python

import argparse
import pandas as pd


parser = argparse.ArgumentParser()
parser.add_argument('in_file', type=str)
parser.add_argument('out_file', type=str)
args = vars(parser.parse_args()) 



dts = ["VAR_0073", "VAR_0075", "VAR_0204", "VAR_0217"]

df = pd.read_csv(args['in_file'])

today = pd.datetime.today()
dt_format = '%d%b%y:%H:%M:%S'
time_since = ['%s_ts' % x for x in dts]
mth = ['%s_month' % x for x in dts]

def convert_year(data, dt_format):
    dt = pd.to_datetime(data, format=dt_format)
    return dt.apply(lambda x: x.month)

df[time_since] = (df[dts].apply(lambda x: today - pd.to_datetime(x, format=dt_format))).apply(lambda x: x / pd.np.timedelta64(1,'D'))
df['73_204_diff'] = df['VAR_0073_ts'] - df['VAR_0204_ts']
df['75_204_diff'] = df['VAR_0075_ts'] - df['VAR_0204_ts']
df['204_217_diff'] = df['VAR_0204_ts'] - df['VAR_0217_ts']

df[mth] = df[dts].apply(lambda x: convert_year(x, dt_format))

df.to_csv(args['out_file'], index=False)
