import sys, subprocess
import pandas as pd 
import numpy as np 

base_dir = '/home/ubuntu/prodrun/concept2'

for run in [1, 2, 3, 4, 5, 6]:
    for tag in ['a', 'b']:
        run_dir = '%s/run%d' % (base_dir, run)
        run_sub_dir = '%s/run%s' % (run_dir, tag) 
        
        print(run_sub_dir)
        
        data_file = '%s/val_df.csv' % run_sub_dir
        
        cmd = 'Rscript scratch/auc_printer.R %s' % run_sub_dir
        subprocess.call(cmd, shell=True)
