import argparse, csv, sys
import pandas as pd 

import settings.common as cfg 

in_file = sys.argv[1]
dense_path = sys.argv[2]
sparse_path = sys.argv[3]

""" In this re-write, assume that all features are dense features and that no conversion 
   of categorical key-value pairs is necessary. The sparse component of the data set 
   is simply a vector of dummy labels. 
 """

print('scanning in data...') 
df = pd.read_csv(in_file)

col_names = list(df.columns.values)
if not 'target' in col_names: 
    df['target'] = 0 

x_vars = [x for x in df.columns.values if not x in ['target', 'ID']]
vars_to_write = ['target'] + x_vars

df2 = df[vars_to_write]    

print('casting sparse and sense matrices...')
with open(dense_path, 'w') as f_d, open(sparse_path, 'w') as f_s: 
    for row in df2.iterrows():
        row = map(str, list(row[1]))  
        f_d.write(' '.join(row) + '\n')
        
        #..write dummy sparse path 
        f_s.write(row[0] + '\n')
