source('settings/r_config.R')
library(xgboost)
library(readr)

args <- commandArgs(trailingOnly = TRUE)
train_file <- args[1]
test_file <- args[2]
out_dir <- args[3]
process_data <- as.integer(args[4])
kill_bad_feats <- as.integer(args[5])
use_good_feats <- as.integer(args[6])
eta                 <- as.numeric(args[7]) #0.02, # 0.06, #0.01,
max_depth           <- as.numeric(args[8]) #8, #changed from default of 8
subsample           <- as.numeric(args[9]) #0.75, # 0.7
colsample_bytree    <- as.numeric(args[10]) #0.75, # 0.7

if(process_data == 1) source('core_data_prep.R')

train_tmp <- (load(train_file))
eval(parse(text = paste('train <-', train_tmp)))
test_tmp <- (load(test_file))
eval(parse(text = paste('test <-', test_tmp)))

print(dim(train))
print(dim(test))

if(kill_bad_feats == 1){
  cat("strip out unhelpful features...\n") #..this list is compiled manually from looking over variable importance results
  features_to_dump <- read_csv('ancillary_files/features_to_dump.csv', col_names = FALSE)[, 1]
  train <- train[!names(train) %in% features_to_dump]
  test <- test[!names(test) %in% features_to_dump]
}

if(use_good_feats == 1){
  cat("only take high-impact features...\n") #..this list is compiled manually from looking over variable importance results
  features_to_keep <- read_csv('ffm/ancillary_files/ffm_vars.csv', col_names = FALSE)[, 1]
  features_to_keep <- c('ID', 'target', features_to_keep)
  train <- train[names(train) %in% features_to_keep]
  test <- test[names(test) %in% features_to_keep]
}

print(dim(train))
print(dim(test))

cat("make sure that all values are numeric and that all integers are numerically expressed:\n")

feature.names <- names(train)[!names(train) %in% c('target', 'ID')]

IntToNum <- function(indata){  
  pass.data <- indata[]  
  pass.data[] <-lapply(pass.data, as.numeric)  
  pass.data
}

train[feature.names] <- IntToNum(train[feature.names])
test[feature.names] <- IntToNum(test[feature.names])

#..Ha, not anymore.... 
#cat("sampling train to get around 8GB memory limitations\n")
#train <- train[sample(nrow(train), 120000),]
#gc()

h <- sample(nrow(train), ceiling(0.75*nrow(train)))

val<-train[-h,]
gc()

train <-train[h,]
gc()

#..We should never have to use this
if(!all(feature.names %in% names(test))){ 
  idx <- feature.names[!feature.names %in% names(test)]
  cat('the features [', paste(feature.names[idx], collapse = ", "), '] are not in the test file and are being dropped from train...\n')
  feats_to_drop <- feature.names[idx]
  train <- train[!names(train) %in% feats_to_drop]
}

dtrain <- xgb.DMatrix(data.matrix(train[,feature.names]), label=train$target)

train=train[1:3,]
gc()

dval <- xgb.DMatrix(data.matrix(val[,feature.names]), label=val$target)
val=val[1:3,]
gc()

watchlist <- watchlist <- list(eval = dval, train = dtrain)

param <- list(  objective           = "binary:logistic", 
                # booster = "gblinear",
                eta                 = eta, # 0.06, #0.01,
                max_depth           = 8, #changed from default of 8
                subsample           = subsample, # 0.7
                colsample_bytree    = colsample_bytree, # 0.7
                eval_metric         = "auc"
                # alpha = 0.0001, 
                # lambda = 1
)

clf <- xgb.train(   params              = param, 
                    data                = dtrain, 
                    nrounds             = 6000, #300, #280, #125, #250, # changed from 300
                    verbose             = 1,
                    early.stop.round    = 100,
                    watchlist           = watchlist,
                    maximize            = TRUE)


dtrain=0
gc()

dval=0
gc()

submission <- data.frame(ID=test$ID)
submission$target <- NA 
for (rows in split(1:nrow(test), ceiling((1:nrow(test))/10000))) {
  submission[rows, "target"] <- predict(clf, data.matrix(test[rows,feature.names]))
  
}

cat("saving the submission file\n")
write_csv(submission, sprintf("%s/xgb.csv", out_dir))

save(clf, file = sprintf('%s/xgb_model.RData', out_dir))
