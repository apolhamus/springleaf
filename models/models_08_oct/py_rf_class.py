#!/usr/bin/env python

from sys import argv
import pandas as pd 
from sklearn.ensemble import RandomForestClassifier 
from sklearn.externals import joblib 

traindata = argv[1]
testdata  = argv[2]
outdir = argv[3]

print "training data: " + traindata
print "testing data: " + testdata

train = pd.read_csv(traindata)
test = pd.read_csv(testdata)

#..TODO (Aaron) This is hacky, fix it
def VowpalFixer(df):
    df = df.rename(columns={'y':'target'})
    df.loc[df['target'] == -1, 'target'] = 0
    return df 

train = VowpalFixer(train)
test = VowpalFixer(test)

classforest = RandomForestClassifier(
n_estimators = 250, 
max_features = 'sqrt', 
min_samples_leaf = 1,
n_jobs = -1)

print "Training classification forest..."
classfit = classforest.fit(X = train.ix[:, 1:train.shape[1]], y = train['target']) 

#print "Saving classification forest..."
#joblib.dump(classfit, outdir + '/rf_class.pkl', compress=9)

print "Generating predictions..."
preds = classfit.predict_proba(test.ix[:, 1:test.shape[1]])[:,1]

print "Writing predictions to test file..."
with open(outdir + '/classrf_py_preds.txt', 'wb') as writer: 
    for item in preds:
        writer.write("%s\n" % item )
