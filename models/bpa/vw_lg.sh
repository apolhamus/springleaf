#!/bin/bash

args=("$@")
traindata=${args[0]}
testdata=${args[1]}
outdir=${args[2]}

echo ${traindata}
echo ${testdata}

#TODO: All of these arguments can be written for command line pass-in . Not sure how worth it it is to play around with bash... 
modelname=${outdir}/lg.vw
lrate=0.287092542426329
#regrate=6.57790743265067e-06
npasses=25
preds_test=${outdir}/lg_vw_preds_test.txt
preds_train=${outdir}/lg_vw_preds_train.txt
cachefile=${outdir}/lg.cache

vw -d ${traindata} --cache_file ${cachefile} -f ${modelname} --passes ${npasses} --loss_function logistic -l ${lrate} -q cz -q cs -q co
vw -d ${testdata} -t -i ${modelname} --link=logistic -p ${preds_test}
vw -d ${traindata} -t -i ${modelname} --link=logistic -p ${preds_train}
