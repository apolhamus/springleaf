#!/usr/bin/env python

from sys import argv
import pandas as pd 
from sklearn.ensemble import RandomForestRegressor 
from sklearn.externals import joblib 

traindata = argv[1]
testdata  = argv[2]
outdir = argv[3]

print "training data: " + traindata
print "testing data: " + testdata

train = pd.read_csv(traindata)
test = pd.read_csv(testdata)

regforest = RandomForestRegressor(
n_estimators = 250, 
max_features = 0.33, 
min_samples_leaf = 100, 
n_jobs = -1)

print "Training regression forest..."
regfit = regforest.fit(X = train.ix[:, 1:train.shape[1]], y = train['target']) 

#print "Saving regression forest..."
#joblib.dump(regfit, outdir + '/rf_reg.pkl', compress=9)

print "Generating predictions..."
preds_test = regfit.predict(test.ix[:, 1:test.shape[1]])
preds_train = regfit.predict(train.ix[:, 1:train.shape[1]])

print "Writing predictions to test file..."
def WriteFunction(outdir, preds, fname):
    with open(outdir + '/' + fname, 'wb') as writer: 
        for item in preds:
            writer.write("%s\n" % item )

WriteFunction(outdir, preds_test, 'regrf_py_preds_test.txt')
WriteFunction(outdir, preds_train, 'regrf_py_preds_train.txt')
