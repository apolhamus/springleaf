import sys, subprocess
import pandas as pd 
from sklearn.grid_search import GridSearchCV
from sklearn.cross_validation import train_test_split
from sklearn.ensemble import RandomForestClassifier 
from sklearn.externals import joblib 

import settings.common as cfg 

format_data_flag = int(sys.argv[1])

if(format_data_flag == 1): 
    print('formatting and scanning in data files...')
    cmd = 'Rscript core_data_prep.R'
    subprocess.call(cmd, shell=True) 
elif(format_data_flag == 0): 
    print('scanning in data files...')
else: 
    print('Set the first argument, format_data_flag equal to 0 or 1')
    sys.exit(1)

train = pd.read_csv(train_data)
test = pd.read_csv(test_data)

classforest = RandomForestClassifier(
n_estimators = 250, 
max_features = 'sqrt', 
min_samples_leaf = 1,
n_jobs = -1)

feature_names = [x for x in train.columns.values if not x in ['target', 'ID']]

print "Training classification forest..."
classfit = classforest.fit(X = train[feature_names], y = train['target']) 

#print "Saving classification forest..."
#joblib.dump(classfit, outdir + '/rf_class.pkl', compress=9)

print "Generating predictions..."
preds_test = classfit.predict_proba(test[feature_names])[:,1]

print "Writing predictions to test file..."
def WriteFunction(outdir, preds, fname):
    with open('%s/%s' % (outdir, fname), 'wb') as writer: 
        for item in preds:
            writer.write("%s\n" % item )

WriteFunction(out_dir, preds_test, 'rf_preds_class.txt')

print "Saving classification forest..."
joblib.dump(classfit, outdir + '/rf_class.pkl', compress=9)

if True: 
    X = train[feature_names]
    y = train['target']
    
    param_grid = [{'min_samples_leaf':[1, 2, 5, 10], 'max_features':['sqrt', 'log2']}, {'min_samples_leaf':[1, 2, 5, 10], 'max_features':[75, 150]}]
    #param_grid = [{'min_samples_leaf':[10, 50], 'max_features':[0.1, 0.25]}]
    clf = GridSearchCV(classfit, param_grid, cv = 3, scoring = 'roc_auc') #..this should be 'roc_auc' fot classification forest
    clf.fit(X, y)
    joblib.dump(clf, '%s/rf_clf_class.pkl' % out_dir, compress=9)
    
    #Valid scoring options are ['accuracy', 'adjusted_rand_score', 'average_precision', 'f1', 'log_loss', 'mean_squared_error', 'precision', 'r2', 'recall', 'roc_auc']
