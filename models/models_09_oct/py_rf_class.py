#!/usr/bin/env python

from sys import argv
import pandas as pd 
from sklearn.ensemble import RandomForestClassifier 
from sklearn.externals import joblib 

traindata = argv[1]
testdata  = argv[2]
outdir = argv[3]

print "training data: " + traindata
print "testing data: " + testdata

train = pd.read_csv(traindata)
test = pd.read_csv(testdata)

classforest = RandomForestClassifier(
n_estimators = 250, 
max_features = 'sqrt', 
min_samples_leaf = 1,
n_jobs = -1)

print "Training classification forest..."
classfit = classforest.fit(X = train.ix[:, 1:train.shape[1]], y = train['target']) 

#print "Saving classification forest..."
#joblib.dump(classfit, outdir + '/rf_class.pkl', compress=9)

print "Generating predictions..."
preds_test = classfit.predict_proba(test.ix[:, 1:test.shape[1]])[:,1]
preds_train = classfit.predict_proba(train.ix[:, 1:train.shape[1]])[:,1]

print "Writing predictions to test file..."
def WriteFunction(outdir, preds, fname):
    with open(outdir + '/' + fname, 'wb') as writer: 
        for item in preds:
            writer.write("%s\n" % item )

WriteFunction(outdir, preds_test, 'classrf_py_preds_test.txt')
WriteFunction(outdir, preds_train, 'classrf_py_preds_train.txt')
