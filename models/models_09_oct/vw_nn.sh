#!/bin/bash

args=("$@")
traindata=${args[0]}
testdata=${args[1]}
outdir=${args[2]}

echo ${traindata}
echo ${testdata}

preds_test=${outdir}/nn_vw_preds_test.txt
preds_train=${outdir}/nn_vw_preds_train.txt
modelname=${outdir}/nn.vw
lrate=0.5
npasses=20
layers=5
cachefile=${outdir}/nn.cache

vw -d ${traindata} --cache_file ${cachefile} --nn ${layers} -f ${modelname} --passes ${npasses} -l ${lrate} --loss_function logistic -q nc -q gc 
vw -d ${testdata} -t -i ${modelname} --link=logistic -p ${preds_test}
vw -d ${traindata} -t -i ${modelname} --link=logistic -p ${preds_train}
