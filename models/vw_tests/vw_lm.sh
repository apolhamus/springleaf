#!/bin/bash

args=("$@")
traindata=${args[0]}
testdata=${args[1]}
outdir=${args[2]}

echo ${traindata}
echo ${testdata}

#TODO: All of these arguments can be written for command line pass-in . Not sure how worth it it is to play around with bash... 
modelname=${outdir}/lm.vw
lrate=0.287092542426329
#regrate=6.57790743265067e-06
npasses=25
preds_test=${outdir}/lm_vw_preds_test.txt
preds_train=${outdir}/lm_vw_preds_train.txt
cachefile=${outdir}/lm.cache

vw -d ${traindata} --cache_file ${cachefile} -f ${modelname} --passes ${npasses} --loss_function squared -l ${lrate} -q nc -q gc
vw -d ${testdata} -t -i ${modelname} -p ${preds_test}
vw -d ${traindata} -t -i ${modelname} -p ${preds_train}
