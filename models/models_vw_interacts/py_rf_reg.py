#!/usr/bin/env python

from sys import argv
import pandas as pd 
from sklearn.ensemble import RandomForestRegressor 
from sklearn.externals import joblib 

traindata = argv[1]
testdata  = argv[2]
outdir = argv[3]

print "training data: " + traindata
print "testing data: " + testdata

train = pd.read_csv(traindata)
test = pd.read_csv(testdata)

#..TODO (Aaron) This is hacky, fix it
def VowpalFixer(df):
    df = df.rename(columns={'y':'target'})
    df.loc[df['target'] == -1, 'target'] = 0
    return df 

train = VowpalFixer(train)
test = VowpalFixer(test)

regforest = RandomForestRegressor(
n_estimators = 250, 
max_features = 0.33, 
min_samples_leaf = 100, 
n_jobs = -1)

print "Training regression forest..."
regfit = regforest.fit(X = train.ix[:, 1:train.shape[1]], y = train['target']) 

#print "Saving regression forest..."
#joblib.dump(regfit, outdir + '/rf_reg.pkl', compress=9)

print "Generating predictions..."
preds = regfit.predict(test.ix[:, 1:test.shape[1]])

print "Writing predictions to test file..."
with open(outdir + '/regrf_py_preds.txt', 'wb') as writer: 
    for item in preds:
        writer.write("%s\n" % item )
