#!/usr/bin/Rscript

args <- commandArgs(trailingOnly = TRUE)

if(!'xgboost' %in% installed.packages()[, 1]) install.packages('xgboost')
require(xgboost)

IntToNum <- function(indata, feature.names){ 
  pass.data <- indata[feature.names]  
  pass.data[] <-lapply(pass.data, as.numeric)  
  pass.data
}

PredFunction <- function(indata, feature.names, file_name){
   preds <- predict(clf, data.matrix(IntToNum(indata, feature.names))) 
   write.table(preds, file.path(outpath, file_name), quote = FALSE, col.names = FALSE, row.names = FALSE)
}

options(scipen = 999)

trainpath <- args[1]
testpath  <- args[2]
outpath   <- args[3]

cat('scanning in data files\n')
traindata <- read.csv(trainpath, as.is = TRUE)
testdata  <- read.csv(testpath, as.is = TRUE)

traindata <- IntToNum(traindata)
testdata <- IntToNum(testdata)

#..Assume that all variables present in the input data frame are feature names
feature.names <- names(traindata)[names(traindata) != 'target']

#..generate index to subset into training and validation frame: 
idx    <- sample(nrow(traindata), as.integer(0.75*nrow(traindata)))
dtrain <- xgb.DMatrix(data.matrix(traindata[idx, feature.names]), label=traindata$target[idx])
dval   <- xgb.DMatrix(data.matrix(traindata[-idx, feature.names]), label=traindata$target[-idx])

watchlist <- list(eval = dval, train = dtrain)

param <- list(  objective           = "binary:logistic", 
                # booster = "gblinear",
                eta                 = 0.001,
                max_depth           = 6,  # changed from default of 6
                subsample           = 0.6,
                colsample_bytree    = 0.6,
                eval_metric         = "auc"
                # alpha = 0.0001, 
                # lambda = 1
)

cat('training xgm model\n')
clf <- xgb.train(   params              = param, 
                    data                = dtrain, 
                    nrounds             = 100, # changed from 300
                    verbose             = 2, 
                    early.stop.round    = 10,
                    watchlist           = watchlist,
                    maximize            = TRUE)

#..this can get memory heavy. may need to chunk it in the future
cat('generating predictions\n')

PredFunction(testdata, feature.names, 'xgbm_r_preds_test.txt')
PredFunction(traindata, feature.names, 'xgbm_r_preds_train.txt')

save(clf, file = file.path(outpath, 'xgbm.RData'))
